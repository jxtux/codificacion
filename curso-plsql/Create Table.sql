--Create Table con Constraint de expresiones regulares
	DROP TABLE contacts;
	CREATE TABLE contacts (
	 l_name VARCHAR2(30),
	 p_number VARCHAR2(30)
	 CONSTRAINT c_contacts_pnf
	 CHECK (REGEXP_LIKE (p_number, '^\(\d{3}\) \d{3}-\d{4}$'))
	);