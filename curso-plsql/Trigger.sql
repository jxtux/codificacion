SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER t_insert_employee
    BEFORE INSERT ON employees
    FOR EACH ROW
    ENABLE
    DECLARE
        v_user VARCHAR2(20);
    BEGIN 
        SELECT user INTO v_user FROM dual;
        DBMS_OUTPUT.PUT_LINE('User que inserto: '||v_user);
END;

INSERT INTO employees(employee_id, hire_date,job_id,email,last_name)
VALUES(500,sysdate,'AD_ASST','cer5x@gmail.com','huin');