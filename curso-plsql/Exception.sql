--1 Exception Oracle
DECLARE 
    v_invalid PLS_INTEGER;

   BEGIN
     v_invalid := 100/0;
     
     EXCEPTION
     WHEN ZERO_DIVIDE THEN
       DBMS_OUTPUT.PUT_LINE ('Attempt to divide by 0');
     WHEN ACCESS_INTO_NULL THEN
       DBMS_OUTPUT.PUT_LINE ('Valor NUll no valid');
      WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE ('Error codigo: '||SQLCODE || ', '|| SQLERRM);
   END;

--2 Exception DE USUARIO Definido como VARIABLE
SET SERVEROUTPUT ON;

DECLARE
    var_dividend NUMBER := 24;
    var_divisor NUMBER := 0;
    var_result NUMBER;
    ex_DivZero Exception;
BEGIN    
    IF var_divisor = 0 THEN
          RAISE ex_DivZero;
    END IF;
    
    var_result := var_dividend/var_divisor;
    dbms_output.put_line('Result: ' || var_result);
    
    EXCEPTION
          WHEN ex_divzero THEN
            dbms_output.put_line('Error exception Division entre Cero');            
END;

--3 Exception de User using RAISE_APPLICATION_ERROR es un procedure interno
 SET SERVEROUTPUT ON
 
 DECLARE
  age NUMBER := 17;
 BEGIN
  IF age < 18 THEN
   RAISE_APPLICATION_ERROR(-20008,'Debe tener 18 a�os o m�s para las bebidas!');
  END IF;

  DBMS_OUTPUT.PUT_LINE('�Por supuesto! Qu� te gustar�a?');

  EXCEPTION WHEN OTHERS THEN
   dbms_output.put_line(SQLERRM); 
 END;
 
--4 Exception de User using RAISE_APPLICATION_ERROR AND procedure interno
 SET SERVEROUTPUT ON
 
 DECLARE
  age NUMBER := 17;
  ex_age EXCEPTION;
  ex_age2 EXCEPTION;
  PRAGMA exception_init(ex_age, -20008);
  PRAGMA exception_init(ex_age2, -20009);
 BEGIN
  IF age < 18 THEN
   RAISE_APPLICATION_ERROR(-20008, 'Debe tener 18 a�os o m�s para las bebidas!');
  END IF;

  DBMS_OUTPUT.PUT_LINE('�Por supuesto! Qu� te gustar�a?');

  EXCEPTION WHEN ex_age THEN
   DBMS_OUTPUT.PUT_LINE(SQLERRM); 
 END;


