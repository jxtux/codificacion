--1. Table Array Anidada
SET SERVEROUTPUT ON;

DECLARE
    TYPE my_table_nested IS TABLE OF NUMBER;
    var_nt my_table_nested := my_table_nested(9,18,17,36,45,54,63,72,81,90);
    
BEGIN    
    FOR I IN 1..var_nt.COUNT 
    LOOP
        dbms_output.put_line('index es: '|| i ||', valor es: ' || var_nt(i));
    END LOOP;
END;

--2. Table Array ASociativa
SET SERVEROUTPUT ON;

DECLARE
    TYPE books IS TABLE OF NUMBER INDEX BY VARCHAR2(20);
    array_books books;
    flag VARCHAR2(20);
    
BEGIN    
    array_books('Oracle PLSQL') := 550;
    array_books('Spring') := 900;
    array_books('JavaEE 8') := 900;
    
    flag := array_books.FIRST;    
    WHILE flag IS NOT NULL
    LOOP
        dbms_output.put_line('Key ->'||flag||', Value -> ' || array_books(flag));
        flag := array_books.NEXT(flag);    
    END LOOP;
    
END;


--3.1 VARRAY BLOCK MIEMBRO
SET SERVEROUTPUT ON;

DECLARE
    TYPE type_varray IS VARRAY(5) OF NUMBER;
    v_vrray type_varray := type_varray();
BEGIN        
    FOR i IN 1..v_vrray.LIMIT
    LOOP
        v_vrray.EXTEND;
        v_vrray(i) := 10*i;
        dbms_output.put_line(v_vrray(i));
    END LOOP;
END;

--3.2 VARRAY TYPE OBJECT DATABASE
SET SERVEROUTPUT ON;

CREATE OR REPLACE TYPE type_array_data IS VARRAY(5) OF NUMBER;

CREATE TABLE calendar5x(
    day_name VARCHAR2(20),
    day_date type_array_data     
);

INSERT INTO calendar5x(day_name,day_date)
VALUES('Sunday',type_array_data(7,14,21,28));

SELECT 
    tab1.day_name,
    vry.COLUMN_VALUE
FROM calendar5x tab1, TABLE(tab1.day_date) vry;

--4. CREATE TABLE CAMPO TYPE TABLE CON REGISTROS TYPE OBJECT
SET SERVEROUTPUT ON;
CREATE OR REPLACE TYPE object_typeX AS OBJECT(
    obj_id NUMBER,
    obj_name VARCHAR2(10)
);

CREATE OR REPLACE TYPE my_table_object IS TABLE OF object_typeX;

CREATE TABLE my_subject_object5x(
    sub_id NUMBER,
    sub_name VARCHAR2(20),
    sub_schedule_day my_table_object     
)NESTED TABLE sub_schedule_day STORE AS nested_space5x;

--CREATE TABLE CAMPO TYPE TABLE
SET SERVEROUTPUT ON;
CREATE OR REPLACE TYPE my_table_nested IS TABLE OF VARCHAR2(10);

CREATE TABLE my_subject_table(
    sub_id NUMBER,
    sub_name VARCHAR2(20),
    sub_schedule_day my_table_nested     
)NESTED TABLE sub_schedule_day STORE AS nested_tab_spacex;

INSERT INTO my_subject_table(sub_id,sub_name,sub_schedule_day)
VALUES(101,'Jose',my_table_nested('rojo','verde'));

INSERT INTO my_subject_table(sub_id,sub_name,sub_schedule_day)
VALUES(101,'Antonio5x',my_table_nested('humano','marciano'));

--BULK COLLECT TABLE
DECLARE
  
   TYPE t_paises IS TABLE OF PAISES%ROWTYPE;
   v_paises t_paises;
 BEGIN
   SELECT  CO_PAIS, DESCRIPCION, CONTINENTE
   BULK COLLECT INTO v_paises
   FROM PAISES;
  
   FOR i IN v_paises.FIRST .. v_paises.LAST LOOP
     dbms_output.put_line(v_paises(i).DESCRIPCION || ', ' || v_paises(i).CONTINENTE);
   END LOOP;
 END;


