-- FORMAS DE EJECUTARSE
	--CON SENTeNCIA SELECT
		SELECT func_ejemplo1 FROM DUAL;
		
	--CON BLOQUE ANONIMO	
		BEGIN
			v_dato = func_ejemplo2 (5001);
		END;
	
--EXAMPLE DE FUNCTION
	CREATE OR REPLACE FUNCTION func_ejemplo1 RETURN empleados.nombre%TYPE AS
	v_dato empleados.nombre%TYPE ;
	v_codigo empleados.empleadoid%TYPE := 0 ;
	BEGIN
		SELECT nombre INTO v_dato FROM EMPLEADOS WHERE
		empleadoid=v_codigo;
		RETURN v_dato;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		RETURN ’ Codigo de empleado no existe!!! ’ ;
	END;