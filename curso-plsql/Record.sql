--1.1 RECORD TABLE BASADO EN UN CAMPO
SET SERVEROUTPUT ON;

DECLARE
    TYPE type_dept IS RECORD(
        f_name VARCHAR2(20),
        d_name departments.department_name%TYPE
    );
    
    v_dept type_dept;
BEGIN    
    SELECT e.first_name,d.department_name
    INTO v_dept.f_name,v_dept.d_name
    FROM employees e JOIN departments d ON e.department_id =d.department_id 
    WHERE e.employee_id = 100;
    
    dbms_output.put_line(v_dept.f_name || ' ' || v_dept.d_name);
END;

--1.2 RECORD TABLE BASADO EN UN REGISTRO MULTIPLE VALORES
SET SERVEROUTPUT ON;

DECLARE
    v_emp employees%ROWTYPE;    
BEGIN    
    SELECT *
    INTO v_emp
    FROM employees WHERE employee_id = 100;
    
    dbms_output.put_line(v_emp.first_name || ' ' || v_emp.hire_date);
END;

--1.3 RECORD TABLE BASADO EN UN REGISTRO ALGUNOS VALORES
SET SERVEROUTPUT ON;

DECLARE
    v_emp employees%ROWTYPE;    
BEGIN    
    SELECT first_name,hire_date  
    INTO v_emp.first_name,v_emp.hire_date 
    FROM employees WHERE employee_id = 100;
    
    dbms_output.put_line(v_emp.first_name || ' ' || v_emp.hire_date);
END;


--2.1 RECORD BASADO EN CURSOR DECLARADO
SET SERVEROUTPUT ON;

DECLARE
    CURSOR cur_rebelion IS
    SELECT first_name,salary
    FROM employees
    WHERE employee_id = 106;
    
    var_emp cur_rebelion%ROWTYPE;
BEGIN     
    OPEN cur_rebelion;
    FETCH cur_rebelion INTO var_emp;    
    dbms_output.put_line(var_emp.first_name || ' ,' ||var_emp.salary);           
    
END;

--2.2 RECORD BASADO EN CURSOR DECLARADO MULTIPLE LOOP
SET SERVEROUTPUT ON;

DECLARE
    CURSOR cur_rebelion IS
    SELECT first_name,salary
    FROM employees
    WHERE employee_id < 106;
    
    var_emp cur_rebelion%ROWTYPE;
BEGIN     
    OPEN cur_rebelion;
    LOOP
        FETCH cur_rebelion INTO var_emp;    
        EXIT WHEN cur_rebelion%NOTFOUND;
        dbms_output.put_line(var_emp.first_name || ' ,' ||var_emp.salary);           
    END LOOP;
    CLOSE cur_rebelion;
END;

--2.3 RECORD NO DECLARADO BASADO EN CURSOR  FOR LOOP
SET SERVEROUTPUT ON;

DECLARE
    CURSOR cur_employee(v_id_emp employees.employee_id%TYPE) IS
        SELECT
            *
        FROM
            employees
        WHERE
            employee_id < v_id_emp;

BEGIN
    FOR ep IN cur_employee(106)
    LOOP
        dbms_output.put_line(ep.first_name || ' ' || ep.last_name);
    END LOOP;

END;


--3.1 DEFINIENDO UN RECORD CON TYPE 
SET SERVEROUTPUT ON;

DECLARE
    TYPE type_dept IS RECORD(
        f_name VARCHAR2(20),
        d_name departments.department_name%TYPE
    );
    
    v_type_dept type_dept;
BEGIN
    SELECT first_name,department_name 
    INTO v_type_dept.f_name,v_type_dept.d_name
    FROM employees JOIN departments USING(department_id)
    WHERE employee_id = 106;
    
     dbms_output.put_line('nombre:'||v_type_dept.f_name||', departamento:'||v_type_dept.d_name);
END;

--3.2 DEFINIENDO UN RECORD CON TYPE Y TABLA CON REGISTROS OCN EL TYPE CREADO
SET SERVEROUTPUT ON;

DECLARE
    TYPE type_dept IS RECORD(
        f_name VARCHAR2(20),
        d_name departments.department_name%TYPE
    );
    
    TYPE table_type_dept IS TABLE OF type_dept;
    
    table_dept table_type_dept;
BEGIN    
    SELECT e.first_name,d.department_name BULK COLLECT INTO table_dept
    FROM employees e JOIN departments d ON e.department_id =d.department_id 
    WHERE e.employee_id < 106;
    
    IF table_dept.COUNT > 0 THEN    
        FOR I IN table_dept.FIRST ..table_dept.LAST LOOP
           dbms_output.put_line('-------------');
           dbms_output.put_line(table_dept(I).f_name);           
           dbms_output.put_line(table_dept(I).d_name);       
        END LOOP;
    END IF;
    
END;
