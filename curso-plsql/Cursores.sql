--ATRIBUTOS DE CURSORES EXPLICITOS: 
	--cursor%NOTFOUND 
	--cursor%FOUND
	--cursor%ROWCOUNT
	--cursor%ISOPEN
	
--ATRIBUTOS DE CURSORES IMPICITOS: 
	--SQL%NOTFOUND 
	--SQL%FOUND
	--SQL%ROWCOUNT
	--cursor%ISOPEN

-- 1 CURSORES EXPLICIOS CON LOOP
SET SERVEROUTPUT ON;

DECLARE
    v_name employees.first_name%TYPE;
    CURSOR cur_employee(v_id_emp employees.employee_id%TYPE) IS
        SELECT
            first_name
        FROM
            employees
        WHERE
            employee_id < v_id_emp;

BEGIN
    OPEN cur_employee(106);
    LOOP
        FETCH cur_employee INTO v_name;
        EXIT WHEN cur_employee%notfound;
        dbms_output.put_line(v_name);
    END LOOP;

    CLOSE cur_employee;
END;

--2 CURSORES EXPLICIOS CON FOR
	SET SERVEROUTPUT ON;

	DECLARE
		v_name employees.first_name%TYPE;
		CURSOR cur_employee(v_id_emp employees.employee_id%TYPE) IS
			SELECT
				first_name,last_name
			FROM
				employees
			WHERE
				employee_id < v_id_emp;

	BEGIN
		FOR ep IN cur_employee(106)
		LOOP
			dbms_output.put_line(ep.first_name || ' ' || ep.last_name);
		END LOOP;

	END;

--CURSOR DE ACTUALIZACIÓN CON CURRENT OF
	DECLARE
		CURSOR c_emp IS 
			SELECT employee_id, last_name, salary
			FROM  employees
			WHERE employee_id = 100
			FOR UPDATE OF salary WAIT 180;         
	BEGIN

		FOR e IN c_emp LOOP
		  UPDATE employees SET salary = 50000 WHERE CURRENT OF c_emp;
		END LOOP;
		
		COMMIT;
		
		EXCEPTION
		  WHEN OTHERS THEN
			ROLLBACK;
	END;
	
--CURSOR SQL DINAMIC

	DECLARE
	  TYPE cur_typ IS REF CURSOR;
	  c_cursor    CUR_TYP;
	  fila PAISES%ROWTYPE;
	  v_query     VARCHAR2(255);
	  codigo_pais VARCHAR2(3) := 'ESP';
	BEGIN

	  v_query := 'SELECT * FROM PAISES WHERE CO_PAIS = :cpais';
	  OPEN c_cursor FOR v_query USING codigo_pais;
	  LOOP
		FETCH c_cursor INTO fila;
		EXIT WHEN c_cursor%NOTFOUND;
		dbms_output.put_line(fila.DESCRIPCION);
	  END LOOP;
	  CLOSE c_cursor;
	END;
	
--ATRIBUTOS DE CURSORES DE CONSULTA
	SET serveroutput ON;
	DECLARE
	BEGIN
	  UPDATE employess2 SET first_name = 'Antonio' 
	  WHERE first_name != 'Alexis';
	  
	  sys.dbms_output.put_line('Afectados: ' || SQL%ROWCOUNT);
	  
	  IF(SQL%FOUND)THEN
		sys.dbms_output.put_line('encontro registros');
	  ELSE
		sys.dbms_output.put_line('no encontro registros');
	  END IF;
	  
	  ROLLBACK;
	END;	