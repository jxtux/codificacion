--FORMAS DE EJECUTARSE
	--Utilizando una sentencia SELECT
	SELECT PACK_EJEMPLO1.func_numero(10) FROM DUAL;
	--Llamándolo desde un bloque de programa o subprograma
	BEGIN
		PACK_EJEMPLO1.proc_municipios;
	END;
	
--PACKAGE
	CREATE OR REPLACE PACKAGE PKG_CONTABILIDAD 
	IS
	   
	  -- Declaraciones de tipos y registros públicas
	  TYPE Cuenta_contable IS RECORD
	  (
		codigo_cuenta VARCHAR2(6),
		naturaleza    VARCHAR2(2) ,
		actividad     VARCHAR2(4) , 
		debe_haber    VARCHAR2(1)
	  );
	  
	  -- Declaraciones de variables y constantes publicas
	  DEBE  CONSTANT VARCHAR2(1) := 'D';
	  HABER CONSTANT VARCHAR2(1) := 'D';
	  ERROR_CONTABILIZAR EXCEPTION;
	  -- Declaraciones de procedimientos y funciones públicas
	  PROCEDURE Contabilizar (mes VARCHAR2) ;
	  FUNCTION  fn_Obtener_Saldo(codigo_cuenta VARCHAR2) RETURN NUMBER;
	END PKG_CONTABILIDAD;

--EXAMPLE
	--DECLARE PUBLIC
	CREATE PACKAGE mipack AS
		PROCEDURE miproc ( v_dato1,v_dato2 ) ;
		FUNCTION mifunc ( v_dato1) RETURN number;
	END ;

	--DECARE BODY DEL PACKAGE
	CREATE PACKAGE BODY mipack AS
		PROCEDURE miproc ( v_dato1,v_dato2 ) as
		BEGIN
		--
		END;
		
		FUNCTION mifunc (v_dato1) RETURN number
		BEGIN
		--
		END;
	END;
	
--Registros PL/SQL estos son
	--	Record
	--	Table
	--	Varray