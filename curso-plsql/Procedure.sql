--FORMAS DE EJECUTARSE UN PROCEDURE
	--Utilizando el comando EXECUTE:
		EXECUTE Proc_ejemplo1;
		
	--Utilizando Bloque anonimo		
		BEGIN
			Proc_ejemplo1;
		END ; 

--EXAMPLE
	CREATE OR REPLACE PROCEDURE proc_ejemplo1( p_codigo in char, p_nombre in out varchar2)AS
		CURSOR V_CURSOR IS SELECT * FROM MUNICIPIOS;
		BEGIN
			FOR v_datos IN v_cursor LOOP
			DBMS_OUTPUT.PUT_LINE('Municipalidad:' || v_datos.nombre );
			END LOOP;
	END;