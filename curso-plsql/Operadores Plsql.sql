SET serveroutput ON;
DECLARE
  v_number1 NUMBER        := 5;
  v_number2 NUMBER        := 10;
  v_nombre1 VARCHAR2(100) := 'Antonio';
  v_nombre2 VARCHAR2(100);
BEGIN
  IF( v_number1 <> v_number2)THEN
    dbms_output.put_line('Son difetentes');
  END IF;
  IF( v_nombre1 LIKE '%to%')THEN
    dbms_output.put_line('Se encontro caracter es SENSITIVE');
  END IF;
  IF( v_number1 between 3 and 10)THEN
    dbms_output.put_line('Esta dentro del parametro de 3 a 10');
  END IF;
  IF( v_number1 in (90,20,5,10))THEN
    dbms_output.put_line('Esta dentro de los valores');
  END IF;
  IF( v_nombre2 IS NULL )THEN
    dbms_output.put_line('Valor es NULL');
  END IF;
END;
