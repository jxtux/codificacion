--FUNCIONES DE GRUPO
SELECT department_id, TRUNC(AVG(salary),2) FROM employees GROUP BY department_id;
SELECT department_id, job_id, sum(salary) FROM employees GROUP BY department_id, job_id ;
SELECT department_id, job_id, sum(salary) FROM employees GROUP BY department_id, job_id 
HAVING SUM(salary) > 20500;

--NVL
SELECT NVL(commission_pct,0) FROM employees;
SELECT NVL(hire_date,sysdate) FROM employees;
SELECT NVL(first_name,'no last name') FROM employees;

--FECHA
SELECT SYSDATE,ADD_MONTHS(SYSDATE,4) AGREGAR_MES , NEXT_DAY(SYSDATE,1) 
AGREGAR_SEMANA, LAST_DAY(sysdate) ULTIMO FROM DUAL;

--SUBCONSULTAS
SELECT first_name, salary FROM employees WHERE salary > (SELECT salary FROM employees WHERE employee_id=105) ;