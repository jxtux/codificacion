--Transacción Autónoma:

	CREATE OR REPLACE PROCEDURE Grabar_Log(descripcion VARCHAR2) 
	IS
	PRAGMA AUTONOMOUS_TRANSACTION;
	BEGIN
	  INSERT INTO LOG_APLICACION
	  (CO_ERROR, DESCRIPICION, FX_ERROR)
	  VALUES
	  (SQ_ERROR.NEXTVAL, descripcion, SYSDATE);
	  COMMIT; 
	END ;

			Transacción Principal:
		
	DECLARE
	  producto PRECIOS%TYPE;
	BEGIN
		 producto := '100599';
		 INSERT INTO PRECIOS 
		 (CO_PRODUCTO, PRECIO, FX_ALTA)
		 VALUES 
		 (producto, 150, SYSDATE);
		 COMMIT;
	EXCEPTION 
	WHEN OTHERS THEN 
		Grabar_Log(SQLERRM);      
		/* Los datos grabados por "Grabar_Log" se escriben en la base
		   de datos a pesar del ROLLBACK, ya que el procedimiento está 
		   marcado como transacción autonoma.
		 */
		ROLLBACK;
	END;
