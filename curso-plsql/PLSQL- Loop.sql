--6 SIMPLE LOOP
SET SERVEROUTPUT ON;

DECLARE
v_counter NUMBER := 0;
v_result NUMBER := 0;
BEGIN
    LOOP
        v_counter := v_counter +1;
        v_result := v_result + v_counter;
        
        DBMS_OUTPUT.PUT_LINE(v_counter+'. '+v_result);
        
        EXIT WHEN v_counter >=10;
    END LOOP;
END;

--7 WHILE LOOP
SET SERVEROUTPUT ON;

DECLARE
v_flag BOOLEAN := TRUE;
v_counter NUMBER := 0;
v_result NUMBER := 0;
BEGIN
    WHILE v_flag LOOP
        v_counter := v_counter +1;
        v_result := v_result + v_counter;        
        DBMS_OUTPUT.PUT_LINE(v_counter||'. '||v_result); 
        
        IF v_counter = 10 THEN
            v_flag := FALSE;
        END IF;
        
    END LOOP;
END;

--8 FOR LOOP
SET SERVEROUTPUT ON;

DECLARE
v_counter NUMBER := 0;
v_result NUMBER := 0;
BEGIN
    FOR v_flag IN 1 .. 10 LOOP
        v_counter := v_counter +1;
        v_result := v_result + v_counter;        
        DBMS_OUTPUT.PUT_LINE(v_counter||'. '||v_result); 
        
    END LOOP;
END;
