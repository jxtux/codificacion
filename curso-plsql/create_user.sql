##############CONECTAR COMO SYSDBA QUE NO TIENE CONTRA
sqlplus / as sysdba

drop user FFNM_KIT cascade;
CREATE USER FFNM_KIT PROFILE "DEFAULT" IDENTIFIED BY "FFNM_KIT" ACCOUNT UNLOCK;

GRANT "CONNECT" TO FFNM_KIT;
GRANT "RESOURCE" TO FFNM_KIT;
GRANT ALTER ANY INDEX TO FFNM_KIT;
GRANT ALTER ANY SEQUENCE TO FFNM_KIT;
GRANT ALTER ANY TABLE TO FFNM_KIT;
GRANT ALTER ANY TRIGGER TO FFNM_KIT;
GRANT CREATE ANY INDEX TO FFNM_KIT;
GRANT CREATE ANY SEQUENCE TO FFNM_KIT;
GRANT CREATE ANY SYNONYM TO FFNM_KIT;
GRANT CREATE ANY TABLE TO FFNM_KIT;
GRANT CREATE ANY TRIGGER TO FFNM_KIT;
GRANT CREATE ANY VIEW TO FFNM_KIT;
GRANT CREATE PROCEDURE TO FFNM_KIT;
GRANT CREATE PUBLIC SYNONYM TO FFNM_KIT;
GRANT CREATE TRIGGER TO FFNM_KIT;
GRANT CREATE VIEW TO FFNM_KIT;
GRANT DELETE ANY TABLE TO FFNM_KIT;
GRANT DROP ANY INDEX TO FFNM_KIT;
GRANT DROP ANY SEQUENCE TO FFNM_KIT;
GRANT DROP ANY TABLE TO FFNM_KIT;
GRANT DROP ANY TRIGGER TO FFNM_KIT;
GRANT DROP ANY VIEW TO FFNM_KIT;
GRANT INSERT ANY TABLE TO FFNM_KIT;
GRANT QUERY REWRITE TO FFNM_KIT;
GRANT SELECT ANY TABLE TO FFNM_KIT;
GRANT UNLIMITED TABLESPACE TO FFNM_KIT;


##############CONECTAR Y CORRAR EL BACKUP EN EL SCHEMA CREADO
connect FFNM_KIT/FFNM_KIT

--IMPORTAR 
@C:\Users\User\Desktop\FFNM_KIT.sql
