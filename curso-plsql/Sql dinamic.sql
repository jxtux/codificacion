--SQL DML DINAMIC

	DECLARE
	  ret NUMBER;
	  FUNCTION fn_execute (nombre VARCHAR2, codigo NUMBER) RETURN NUMBER 
	  IS
		sql_str VARCHAR2(1000);
	  BEGIN
		sql_str := 'UPDATE DATOS SET NOMBRE = :new_nombre 
					WHERE CODIGO = :codigo';    
		EXECUTE IMMEDIATE sql_str USING nombre, codigo;   
		RETURN SQL%ROWCOUNT;
	  END fn_execute ;
	BEGIN
		 ret := fn_execute('Devjoker',1);
		 dbms_output.put_line(TO_CHAR(ret));
	END;
	
--CURSORES SQL DINAMIC
	DECLARE
	  TYPE cur_typ IS REF CURSOR;
	  c_cursor    CUR_TYP;
	  fila PAISES%ROWTYPE;
	  v_query     VARCHAR2(255);
	  codigo_pais VARCHAR2(3) := 'ESP';
	BEGIN

	  v_query := 'SELECT * FROM PAISES WHERE CO_PAIS = :cpais';
	  OPEN c_cursor FOR v_query USING codigo_pais;
	  LOOP
		FETCH c_cursor INTO fila;
		EXIT WHEN c_cursor%NOTFOUND;
		dbms_output.put_line(fila.DESCRIPCION);
	  END LOOP;
	  CLOSE c_cursor;
	END;