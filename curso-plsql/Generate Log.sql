--CREAR CARPETA COMO SYSTEM
CREATE OR REPLACE DIRECTORY RUTA_LOG AS 'D:\ORACLE\Workspace';

--DAR PERMISOS DE READ Y WRITE AL USUARIO QUE UTILIZARA LA CARPETA
GRANT ALL ON DIRECTORY RUTA_LOG TO HR;

--Revisar si el package existe
select * from all_objects where object_name = 'UTL_FILE' 

--Dar permisos de ejecucion al package UTL_FILE
sqlplus / as sysdba
GRANT EXECUTE ON SYS.utl_file TO your_db_username;

--DECLARATE
CREATE OR REPLACE PACKAGE pkg_log AS
   PROCEDURE anadir_log(archivo IN VARCHAR2,cadena IN VARCHAR2);
END pkg_log;

--BODY
create or replace PACKAGE BODY pkg_log AS

 PROCEDURE anadir_log(archivo IN VARCHAR2,cadena IN VARCHAR2) AS
    file_log UTL_FILE.FILE_TYPE; 

  BEGIN 
    file_log := UTL_FILE.FOPEN('RUTA_LOG',archivo,'A',256);
    UTL_FILE.PUT(file_log,cadena); 
    UTL_FILE.FCLOSE(file_log);  
    
  EXCEPTION
    WHEN utl_file.access_denied THEN
      dbms_output.put_line ('Error: No tiene permiso de lectura');
    WHEN utl_file.invalid_operation THEN
      dbms_output.put_line ('Error: Archivo abierto o bloqueado o no existe');
    WHEN OTHERS THEN
      dbms_output.put_line ('Error al escribir en file');
  END anadir_log;
  
END pkg_log;

--EXECUTE
set SERVEROUTPUT ON;
DECLARE
  prefijo VARCHAR2(100) := 'log_sistema5x_hr_';
  sufijo VARCHAR2(100) := '.log';
  fecha VARCHAR2(100);
  archivo VARCHAR2(100);
BEGIN
  select to_char( sysdate, 'DDMMYYYY' ) INTO fecha from dual;
  archivo := prefijo||fecha||sufijo;
  pkg_log.anadir_log(archivo,'Guardar LOG 5x');  
END;
