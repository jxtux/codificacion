--1 VARIIBLES
SET SERVEROUTPUT ON;

DECLARE
    v_test   VARCHAR2(55);
BEGIN
    v_test   := 'joseantonio5x@gmail.com';
    dbms_output.put_line(v_test);
END;

--2 INTO CURSORES IMPLICITOS Y  Datatype(%TYPE)

SET SERVEROUTPUT ON;

DECLARE
    v_test   VARCHAR2(15);
BEGIN
    v_test   := 'joseantonio5x@gmail.com';
    dbms_output.put_line(v_test);
END;

--3 CONSTANT(DECLARADO EN UN PACKAGE)

SET SERVEROUTPUT ON;

DECLARE
    constante   CONSTANT NUMBER := 100;
BEGIN
    dbms_output.put_line(TO_CHAR(pkg_variables_globales.get_pi) );
    dbms_output.put_line(TO_CHAR(constante) );
END;

--4 VARIABLE

SET SERVEROUTPUT ON;

VARIABLE v_bind VARCHAR2(20);

DECLARE BEGIN
    :v_bind   := 'JOSE ANTONIO';
    dbms_output.put_line(:v_bind);
END;

--VARIABLE DE HOST NO DE PLSQL INIICALIZADO CON EXEC

SET AUTOPRINT OFF;

VARIABLE v_bind VARCHAR2(30);

EXEC :v_bind := 'cero.one.x@gmail.com';
    
--5 ELSE IF
SET SERVEROUTPUT ON;

DECLARE
v_place VARCHAR2(20) := 'Lima';
BEGIN
    IF v_place = 'Lima' THEN
        dbms_output.put_line('Lima');
    ELSIF v_place = 'Piura' THEN
        dbms_output.put_line('Piura');
    ELSIF v_place = 'Tacna' THEN
        dbms_output.put_line('Tacna');
    ELSE
        dbms_output.put_line('Otros');
    END IF;
END;