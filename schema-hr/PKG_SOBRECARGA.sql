CREATE OR REPLACE PACKAGE pkg_sobrecarga IS
    FUNCTION func_letras_numeros(p_param NUMBER)
        RETURN VARCHAR2;

    FUNCTION func_letras_numeros(p_param VARCHAR2)
        RETURN VARCHAR2;
END;
/


CREATE OR REPLACE PACKAGE BODY pkg_sobrecarga IS
    FUNCTION func_letras_numeros(p_param NUMBER)
        RETURN VARCHAR2 IS
        v_param_length  NUMBER;
        v_msg           VARCHAR2(500);
        v_type          VARCHAR2(50);
     BEGIN
        v_param_length  :=  LENGTH(p_param);
        v_type          := 
                         CASE
                            WHEN v_param_length<2 THEN 'un d�gito.'
                            ELSE v_param_length||' d�gitos.'
                         END;
        v_msg           :=  ', Tipo: Num�rico, con una dimensi�n de '||v_type;

        RETURN p_param||v_msg;
     END func_letras_numeros;
--
    FUNCTION func_letras_numeros(p_param VARCHAR2)
        RETURN VARCHAR2 IS
        v_param_length  NUMBER;
        v_msg           VARCHAR2(500);
        v_type          VARCHAR2(50);
     BEGIN
        v_param_length  := LENGTH(p_param);
        v_type          := 
                         CASE
                            WHEN v_param_length<2 THEN 'un car�cter.'
                            ELSE v_param_length||' Caracteres.'
                         END;
        v_msg           := ', Tipo: Car�cter, con una dimensi�n de '||v_type;

        RETURN p_param||v_msg;
     END func_letras_numeros;
END pkg_sobrecarga;
/
