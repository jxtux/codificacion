CREATE OR REPLACE PACKAGE test_pkg AS
   TYPE typeCur IS REF CURSOR;
   
   PROCEDURE in_only_test (inParam1 IN VARCHAR2);
   PROCEDURE in_and_out_test (inParam1 IN VARCHAR2, outParam1 OUT VARCHAR2);
   PROCEDURE listEmployeeDept(deptId IN NUMBER, listEmpleado OUT typeCur);
   PROCEDURE listEmployeeDeptParam(deptId IN NUMBER, listEmpleado OUT typeCur);
END test_pkg;
/


CREATE OR REPLACE PACKAGE BODY test_pkg AS

   PROCEDURE in_only_test(inParam1 IN VARCHAR2) AS
   BEGIN
      DBMS_OUTPUT.PUT_LINE('in_only_test');
   END in_only_test;
   
   PROCEDURE in_and_out_test(inParam1 IN VARCHAR2, outParam1 OUT VARCHAR2) AS
   BEGIN
      outParam1 := 'Woohoo Im an outparam, and this is my inparam ' || inParam1;
   END in_and_out_test;
   
   PROCEDURE listEmployeeDept(deptId IN NUMBER, listEmpleado OUT typeCur) AS
   sql_query VARCHAR2(300);
   
   BEGIN        
       sql_query := 'Select
        EMPLOYEE_ID,
        FIRST_NAME,
        LAST_NAME,
        EMAIL,
        PHONE_NUMBER,
        HIRE_DATE,
        JOB_ID,
        SALARY,
        COMMISSION_PCT,
        MANAGER_ID,
        DEPARTMENT_ID
        from EMPLOYEES where DEPARTMENT_ID =:v AND FIRST_NAME IS NOT NULL'; 

        OPEN listEmpleado FOR sql_query
        USING deptId;       
   END listEmployeeDept;
   
  PROCEDURE listEmployeeDeptParam(deptId IN NUMBER, listEmpleado OUT typeCur) AS
   sql_query VARCHAR2(300);
   
   BEGIN        
       sql_query := 'Select
        EMPLOYEE_ID id,
        FIRST_NAME firstName,
        LAST_NAME lastName
        from EMPLOYEES where DEPARTMENT_ID =:v AND FIRST_NAME IS NOT NULL'; 

        OPEN listEmpleado FOR sql_query
        USING deptId;       
   END listEmployeeDeptParam;
   
END test_pkg;
/
