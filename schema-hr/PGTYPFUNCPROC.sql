CREATE OR REPLACE PACKAGE pgTypFuncProc AS 
   --Declaracion de variables publicas o GLobales1
     var_name_default VARCHAR(100) := 'antonio5x';
     var_edad_default CONSTANT NUMBER(3) := 70;
     r_empleado EMPLOYEES%ROWTYPE;
    
   --Definicion de Tipo
     --Record 
     TYPE typeRecEmp IS RECORD ( employee_id employees.employee_id%TYPE, nombre VARCHAR2(60)); 
     
     --Tabla Asociativa,Tabla Anidada(Sin index) y Tabla estatica 
     TYPE typeTableVarchar  IS TABLE OF VARCHAR2(60) INDEX BY PLS_INTEGER;   
     TYPE typeTableEmp IS TABLE OF employees%ROWTYPE  INDEX BY PLS_INTEGER;
     TYPE typeTableEmpEmail IS TABLE OF employees.email%TYPE; 
     TYPE typeTableSarray IS VARRAY(5) OF VARCHAR2(10); 
     TYPE typeTableEmV IS VARRAY(5) OF employees%ROWTYPE; 
   
   --Prototipos de  los cursores publicas
     TYPE typeCur IS REF CURSOR;
     TYPE typeCurEmp IS REF CURSOR RETURN employees%ROWTYPE;  

   --Prototipos de los procedimientos y funcioes publicas
     FUNCTION fn_obtenerComissionAvg (depId IN NUMBER) RETURN EMPLOYEES%ROWTYPE; 
     PROCEDURE pr_nombre_correcto(codigo NUMBER,message VARCHAR2,user VARCHAR2);
     PROCEDURE pr_SelectEmployee(departamentoId IN NUMBER,cursoEmpleado IN OUT typeCur);
   
END pgTypFuncProc;
/


CREATE OR REPLACE PACKAGE BODY pgTypFuncProc AS
  vari_typeRecEmp typeRecEmp;
 
    FUNCTION fn_obtenerComissionAvg(depId IN NUMBER)
    RETURN EMPLOYEES%ROWTYPE IS
		  comisionAvg NUMBER(4);      
      insertCod NUMBER(4);
		  
	  BEGIN    
		  SELECT * INTO r_empleado
		  FROM EMPLOYEES 
		  WHERE EMPLOYEE_ID=105;
      
      INSERT INTO COMISION_ERROR(CODIGO_ERROR,MENSAJE_ERROR,USUARIO,FECHA)  
      VALUES( r_empleado.EMPLOYEE_ID, 'AVG calculado', 'HR', SYSTIMESTAMP)
      RETURNING CODIGO_ERROR INTO insertCod;
		  
      COMMIT;
		  RETURN r_empleado;
	  END fn_obtenerComissionAvg;
	
    PROCEDURE pr_nombre_correcto(codigo NUMBER,message VARCHAR2,user VARCHAR2) AS
      PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO COMISION_ERROR VALUES( codigo, message, user, SYSTIMESTAMP);
        COMMIT;
    END pr_nombre_correcto;
  
    PROCEDURE pr_SelectEmployee(departamentoId IN NUMBER,cursoEmpleado IN OUT typeCur) AS
    
    BEGIN
      OPEN cursoEmpleado FOR ' Select * from EMPLOYEES where DEPARTMENT_ID =:v'
      using departamentoId; 

    END  pr_SelectEmployee;
END pgTypFuncProc;
/
