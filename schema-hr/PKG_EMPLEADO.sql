CREATE OR REPLACE PACKAGE pkg_empleado IS

    TYPE  typ_emp_details IS RECORD (
          nombre              VARCHAR2(60),
          tiempo_vig          NUMBER(2),  --Tiempo Vigente en la empresa
          departamento_act    hr.departments.department_name%TYPE,
          empleo              hr.jobs.job_title%TYPE,
          direccion           VARCHAR2(150)
      );

    TYPE  typ_empleo_hist IS TABLE OF jobs.job_title%TYPE INDEX BY pls_integer;

    TYPE  typ_dept_hist IS TABLE OF departments.department_name%TYPE INDEX BY pls_integer;

    PROCEDURE proc_emp_details(   p_cod_emp IN employees.employee_id%TYPE, p_emp_details OUT typ_emp_details );

    FUNCTION func_emp_hist( p_cod_emp IN employees.employee_id%TYPE ) RETURN typ_empleo_hist;
END pkg_empleado;
/



/
