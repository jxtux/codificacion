CREATE OR REPLACE PACKAGE test_package_oracle AS 
   FUNCTION fn_obtenerComissionAvg (depId IN NUMBER) RETURN NUMBER; 
   PROCEDURE pr_ActualizarComision(departamentoId IN NUMBER,comision IN NUMBER,estado OUT VARCHAR2);
END test_package_oracle;
/


CREATE OR REPLACE PACKAGE BODY test_package_oracle AS
	FUNCTION fn_obtenerComissionAvg
	  (depId IN NUMBER)
	RETURN NUMBER
	  IS
		  comisionAvg NUMBER(4,2);
		  
	  BEGIN    
		  SELECT AVG(COMMISSION_PCT) 
		  INTO comisionAvg
		  FROM EMPLOYEES 
		  WHERE DEPARTMENT_ID = depId AND COMMISSION_PCT IS NOT NULL;
		  
		  RETURN comisionAvg;
	  END;
	  	  
  PROCEDURE pr_ActualizarComision
	(departamentoId IN NUMBER,comision IN NUMBER,estado OUT VARCHAR2) AS
	BEGIN
    SYS.DBMS_OUTPUT.PUT_LINE('DepartamentoId no valido: '||TO_CHAR(departamentoId));
	  IF(departamentoId != 0)THEN
      UPDATE EMPLOYEES 
      SET COMMISSION_PCT = comision
      WHERE DEPARTMENT_ID = departamentoId;
      estado := 'ok';
    ELSE
      estado := 'Departamento no existe';
    END IF;
	  
	EXCEPTION
	  WHEN OTHERS THEN
	  estado := 'El procedimiento fallo' || sqlerrm;
	END;
END test_package_oracle;
/
